#---------------------------------------------------

import numpy as np

def npa_get_starting_model(xmin, xmax, ymin, ymax, zmin, zmax, vpmin, vpmax, vsmin, vsmax):
    
    X0=np.random.uniform(low=xmin,high=xmax)
    Y0=np.random.uniform(low=ymin,high=ymax)
    Z0=np.random.uniform(low=zmin,high=zmax)
    VP0=np.random.uniform(low=vpmin,high=vpmax)
    VS0=np.random.uniform(low=vsmin,high=vsmax)
    
    return X0, Y0, Z0, VP0, VS0

#---------------------------------------------------

from math import sqrt

stlo = []
stla = []

def npa_LOCATE_syn(nstat_sele, stlo, stla, X0, Y0, Z0, VP0, VS0):
    
    SYN_DATA_P = []
    SYN_DATA_S = []
    istat=0
    while istat < nstat_sele:
        x1=float(stlo[istat])
        y1=float(stla[istat])
        z1=0.0
        dist= sqrt( ((X0-x1)*111.19)**2 + ((Y0-y1)*111.19)**2 + (Z0-z1)**2 )
        theo_P = (1.0/VP0) * dist 
        theo_S = (1.0/VS0) * dist
        #print(istat,dist,theo_P,theo_S)
        SYN_DATA_P.append(theo_P)
        SYN_DATA_S.append(theo_S)
        istat += 1

        
    return SYN_DATA_P, SYN_DATA_S

#---------------------------------------------------

Ppick = []
Spick = []
SYN_DATA_P = []
SYN_DATA_S = []

def npa_log_likelihood(nstat_sele, SYN_DATA_P , SYN_DATA_S, Ppick, Spick, sigma0):
    
    lppd=0
    id=0
    while id < nstat_sele:
        lppd = lppd + ((SYN_DATA_P[id] - Ppick[id])/sigma0)**2
        lppd = lppd + ((SYN_DATA_S[id] - Spick[id])/sigma0)**2
        #print(id,lppd,SYN_DATA_P[id],Ppick[id],SYN_DATA_S[id],Spick[id])
        id+=1
        
    return lppd

#---------------------------------------------------

import numpy as np

def npa_candidate_model(X0, Y0, Z0, VP0, VS0, xmin, xmax, ymin, ymax, zmin, zmax, vpmin, vpmax, vsmin, vsmax):
    
    
    X_cand=X0
    Y_cand=Y0
    Z_cand=Z0
    VP_cand=VP0
    VS_cand=VS0
    
    p=np.random.uniform(low=0.0,high=1.0)
    
    if p < 0.20:
# Pertub X
        p=np.random.uniform(low=0.0,high=1.0)
        if p < 0.5:
            X_cand=np.random.uniform(low=xmin,high=xmax)
        else:
            X_cand=npa_uniform_rand_walk(X0,xmin,xmax)

    if p >= 0.20 and p < 0.40:
# Perturb Y
        p=np.random.uniform(low=0.0,high=1.0)
        if p < 0.5:
            Y_cand=np.random.uniform(low=ymin,high=ymax)
        else:
            Y_cand=npa_uniform_rand_walk(Y0,ymin,ymax)

    if p >= 0.40 and p < 0.60:
# Perturb Z
        p=np.random.uniform(low=0.0,high=1.0)
        if p < 0.5:
            Z_cand=np.random.uniform(low=zmin,high=zmax)
        else:
            Z_cand=npa_uniform_rand_walk(Z0,zmin,zmax)

    if p >= 0.60 and p < 0.80:
# Perturb VP
        p=np.random.uniform(low=0.0,high=1.0)
        if p < 0.5:
            VP_cand=np.random.uniform(low=vpmin,high=vpmax)
        else:
            VP_cand=npa_uniform_rand_walk(VP0,vpmin,vpmax)
            
    if p >= 0.80:
# Perturb VS        
        p=np.random.uniform(low=0.0,high=1.0)
        if p < 0.5:
            VS_cand=np.random.uniform(low=vsmin,high=vsmax)
        else:
            VS_cand=npa_uniform_rand_walk(VS0,vsmin,vsmax)
        
    return X_cand, Y_cand, Z_cand, VP_cand, VS_cand


#---------------------------------------------------


def npa_uniform_rand_walk(x,xmin,xmax):

    x_new=x
    
    sc=0.05
    halfwidth=0.5*sc*(xmax-xmin)
    xcandhigh=x+halfwidth
    if xcandhigh > xmax:
        xcandhigh=xmax
    xcandlow=x-halfwidth
    if xcandlow <xmin:
        xcandlow=xmin
    maxdev=xcandhigh-xcandlow
    dev=np.random.uniform(low=0.0,high=1.0)
    xcand=xcandlow+dev*maxdev
    xhigh=xcand+halfwidth
    if xhigh > xmax:
        xhigh=xmax
    xlow=xcand-halfwidth
    if xlow < xmin:
        xlow=xmin
    probacc=(xcandhigh-xcandlow)/(xhigh-xlow)
    dev=np.random.uniform(low=0.0,high=1.0)
    if probacc > dev:
        x_new=xcand

    return x_new


#---------------------------------------------------

import math
import numpy as np

def npa_metropolis(lppd, lppd0):
    
    p=np.random.uniform(low=0.0,high=1.0)
    
    a=lppd-lppd0
    if a > 12.0:
        a = 12.0
    if a < -12.0:
        a = -12.0
                
    alpha = math.exp( -0.5 * a )
    
    if alpha >= 1.0:
        
        Accepted = 1
      
    if alpha < 1.0:
        
        if p <= alpha:
            
            Accepted = -1
            
        else:
        
            Accepted = 0

    return Accepted


#---------------------------------------------------



