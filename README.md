# UNIBO-INV-PROBL-2023

Jupyter Notebook per il Problema Inverso della localizzazione di un evento sismico


[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Funibo-inv-probl-2023/HEAD?labpath=UNIBO_TIP_03_locate_seismic_event_TRIAL_AND_ERROR.ipynb)


